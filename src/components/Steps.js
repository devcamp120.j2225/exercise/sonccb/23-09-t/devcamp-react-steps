import { Component } from "react";
import {steps} from '../data/data.js'

class Steps extends Component {
     render() {
          return (
               <div>
                    <ul>
                         {
                         steps.map((value,index)=>{
                              return <li key={index} id={value.id} title={value.title}>{value.content}</li>
                         })
                         }
                    </ul>
               </div>
          )
     }
}

export default Steps;
